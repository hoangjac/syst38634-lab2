package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
		
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("the time provided is not valid");
		
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		
		int totalSeconds = Time.getTotalSeconds("00:00:00");
		
		assertTrue("The time provided does not match the result", totalSeconds == 0);
		
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("the time provided is not valid");
		
	}
	
	

}
